﻿using UnityEngine;

public class ResourcesController : MonoBehaviour
{
    [SerializeField]
    private int _maxNumberShovels = 100;

    private UIController _uiCnt = null;

    public int CurrentNumShovels { get; private set; } = 0;

    private void Start()
    {
        CurrentNumShovels = _maxNumberShovels;
        _uiCnt = GetComponent<UIController>();
        _uiCnt.UpdateShovelCounter(_maxNumberShovels);
    }

    public bool SpendShovel()
    {
       
        if (CurrentNumShovels-1>=0)
        {
            CurrentNumShovels--;
            _uiCnt.UpdateShovelCounter(CurrentNumShovels);
            return true;
        }
        else
        {
            return false;
        }
       
    }

    public void AddShovel(int count)
    {
        CurrentNumShovels += count;
        _uiCnt.UpdateShovelCounter(CurrentNumShovels);
    }


}
