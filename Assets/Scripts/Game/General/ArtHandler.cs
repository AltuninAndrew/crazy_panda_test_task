﻿using UnityEngine;
using UnityEngine.EventSystems;

public class ArtHandler : MonoBehaviour,IBeginDragHandler,IDragHandler,IEndDragHandler
{
    private Vector3 _origPosition;

    [SerializeField]
    private int _worth = -1;

    public void OnBeginDrag(PointerEventData eventData)
    {
        _origPosition = transform.position;
        transform.parent.gameObject.GetComponent<CellHandler>().IsClickable = true;

    }

    void IDragHandler.OnDrag(PointerEventData eventData)
    {
        transform.position = Camera.main.ScreenToWorldPoint(new Vector3(eventData.position.x,eventData.position.y,1));
    }

    public void OnEndDrag(PointerEventData eventData)
    {
        transform.position = _origPosition;
        transform.parent.gameObject.GetComponent<CellHandler>().IsClickable = false;
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.gameObject.name == "Backpack")
        {
            collision.gameObject.GetComponent<BackpackController>().FillUpBackpack(_worth);
            
            Destroy(gameObject);
        }
    }

}
