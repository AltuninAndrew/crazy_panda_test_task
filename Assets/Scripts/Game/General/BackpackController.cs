﻿using UnityEngine;

public class BackpackController : MonoBehaviour
{
    public int NumberOfPoints  { get; private set; }

    private UIController _uiCnt = null;

    private void Start()
    {
       _uiCnt = GameObject.FindGameObjectWithTag("GameController").GetComponent<UIController>();
    }

    public void FillUpBackpack(int artWorth)
    {
        NumberOfPoints += artWorth;

        if (_uiCnt!=null)
        {
            _uiCnt.UpdatePointsCounter(NumberOfPoints);
        }
        
    }

    public bool TakeFromBackpack (int count)
    {
        if((NumberOfPoints - count)>=0)
        {
            NumberOfPoints -= count;
            _uiCnt.UpdatePointsCounter(NumberOfPoints);
            return true;
        }
        else
        {
            return false;
        }
    }



}
