﻿using UnityEngine;
public class BtnBuyShovel : MonoBehaviour
{
    [SerializeField]
    private int _worthShovel = 100;

    [SerializeField]
    private ResourcesController _resourcesController = null;
    [SerializeField]
    private BackpackController _backpackController = null;

    public void Buy()
    {
       if(_backpackController.TakeFromBackpack(_worthShovel))
        {
            _resourcesController.AddShovel(5);
        }
    }
    
    

}
