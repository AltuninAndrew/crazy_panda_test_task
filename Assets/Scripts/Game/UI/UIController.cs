﻿using UnityEngine;
using UnityEngine.UI;

public class UIController : MonoBehaviour
{
    [SerializeField]
    Text _txtPoints = null;
    [SerializeField]
    Text _txtShovel = null;


    public void UpdatePointsCounter(int count)
    {
        if(_txtPoints !=null)
        {
            _txtPoints.text = $"Total points: {count}";
        }
    }

    public void UpdateShovelCounter(int count)
    {
        _txtShovel.text = $"x{count}";
    }

}
