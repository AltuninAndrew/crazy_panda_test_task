﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class BtnExitMenu : MonoBehaviour
{
    public void ExitMenu()
    {
        SceneManager.LoadScene(0);
    }
}
