﻿using UnityEngine;

public class CellGenerator : MonoBehaviour
{
    [SerializeField]
    private GameObject _prefab = null;

    [SerializeField]
    private int _numSockets = 100;

    private ResourcesController _resourcesController;

    void Start()
    {
        _resourcesController = GameObject.Find("GameController").GetComponent<ResourcesController>();

        if(_prefab !=null)
        {
            for (var i = 0; i < _numSockets; i++)
            {
                GameObject socket = Instantiate(_prefab,gameObject.transform);
                socket.GetComponent<CellHandler>().InitResourcesCnt(_resourcesController);
                
                socket.name = $"Cell {i.ToString()}";
            }
        }
        
    }

}
