﻿using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class CellHandler : MonoBehaviour, IPointerClickHandler
{

    [SerializeField]
    private int _maxDeep = 4;

    [SerializeField]
    private int _maxNumberArtifacts = 3; 

    [SerializeField]
    private string _pathToArtifacts = "Prefabs/Artifacts/art";

    [SerializeField]
    private string _pathToGround = "Sprites/Ground/ground";

    private Image _img;


    public bool IsClickable { get; set; } = true;
    public int CurrentDeep { get; private set; } = 0;

    private ResourcesController _resourcesController = null;

    public void InitResourcesCnt(ResourcesController resourcesController)
    {
        _resourcesController = resourcesController;
    }

    private void Start()
    {
        _img = GetComponent<Image>();
    }

    public void OnPointerClick(PointerEventData eventData)
    {
        if (IsClickable)
        {
            TouchEvent();
        }
        
    }

    private void TouchEvent()
    {

        CurrentDeep++;
        
        if(CurrentDeep<=_maxDeep &&_resourcesController.SpendShovel())
        {
            string path = $"{_pathToGround}{CurrentDeep}";
            _img.overrideSprite = Resources.Load<Sprite>(path);
            AppearanceArtefacts();
        }
        else
        {
            //Debug.Log("Deep is max");       
        }
       
        
    }

    private void AppearanceArtefacts()
    {
        int chance = Random.Range(0, 2);
        
        if(chance>0)
        {   
            int typeArtifact = Random.Range(0, _maxNumberArtifacts);
            GameObject obj = Instantiate(Resources.Load<GameObject>($"{_pathToArtifacts}{typeArtifact}"),gameObject.transform);
            obj.transform.position = new Vector3(obj.transform.position.x, obj.transform.position.y, 1);


            IsClickable = false;
        }

    }

    
    
}
